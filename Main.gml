// This is how we define scripts (aka methods, functions). "Main" is a script
// that your mod MUST have, and it gets called as soon as the mod is booted up.
#define Main
  
  #region Basics

  // This is how you print a message to the in game console.
  Trace("Hello world! This is a message from the Example Mod.");

  // This is how you create a command for the chat. This command would be
  // recognized as "/greeting", and would call the PrintGreeting. You can
  // see what PrintGreeting actually is at the bottom of this file.
  // It's important to note that any time you pass around a script while
  // modding, you HAVE to use ScriptWrap!
  CommandCreate("greeting", false, ScriptWrap(PrintGreeting));

  // ModFind lets you try to find an active mod's ID by supplying its name
  // (the one listed in its config). This is the only time you'll reference
  // a mod by its name -- all other times you must use its unique ID.
  var _myMod = ModFind("Forager Example Mod");

  // Now that we've found an ID, we can read a global variable from this mod.
  // For the sake of this example, I'm reading a global from this mod
  // specifically which you normally wouldn't do.
  globalvar testVariable;
  testVariable = 1;
  var _foundValue = ModGlobalGet(_myMod, "testVariable"); 

  // I can also add my own keys to the localization here. Let's add the name
  // and descriptin of an item I'm going to create to the enlgish
  // localization. You should know that all displayed text in Forager should
  // be lower case!
  LocalizationAddKey("english", "magicWandName", "magic wand");
  LocalizationAddKey("english", "magicWandDescription", "a mystical little stick capable of great power.")

  #endregion

  #region Sprites

  // For adding sprites and sounds, the functions will look in your mods
  // directory before normal GameMaker behavior. So, to get my sprite
  // located in '/mods/Forager Example Mod/Resources/', I can just type
  // 'Resources/'
  var _wandSprite = sprite_add("Resources/sprMagicWand.png", 1, false, false, 8, 8);

  #endregion

  #region Items and Gear

  // This allows you to create a new item in Forager. All of its parameters
  // are optional, meaning you can stop adding parameters whenever you like,
  // and let the default values take over. More information on the function
  // and its parameters can be found on the documentation.
  globalvar ItemMagicWand;
  ItemMagicWand = ItemCreate(
    undefined,
    Localize("magicWandName"),
    Localize("magicWandDescription"),
    _wandSprite,
    ItemType.Gear,
    ItemSubType.None,
    100,
    0,
    0,
    [Item.Wood, 50, Item.StarFragment, 20],
    ScriptWrap(UseMagicWand)
  );

  // Actually, I chaged my mind -- let's change the blueprint!
  ItemEdit(ItemMagicWand, ItemData.Blueprint, [Item.Wood, 10, Item.StarFragment, 10]);

  // Now that we have our item, let's put it in its own Gear Category.
  globalvar GearMagicWand;
  GearMagicWand = GearCategoryCreate(undefined, "magic wand", true, true);
  GearCategoryAddItems(GearMagicWand, ItemMagicWand);

  #endregion

  #region Structures

  // Now, let's create a structure that will produce our wand.
  globalvar StructureWandMaker;
  StructureWandMaker = StructureCreate(
    undefined,
    "wand maker",
    "it makes wands!",
    StructureType.Base,
    sprForge,
    undefined,
    [Item.Steel, 10],
    1,
    true,
    [ItemMagicWand],
    true,
    BuildMenuCategory.Magical,
    [GearMagicWand]
  );

  // Nah -- let's change the blueprint here too!
  StructureEdit(StructureWandMaker, StructureData.Blueprint, [Item.Steel, 50]);

  #endregion

  #region Enemies

  // Now we can create our own custom type of enemy that we can spawn in the game!
  globalvar EnemyLazySkeleton;
  EnemyLazySkeleton = EnemyCreate(
    undefined,
    "lazy skeleton",
    EnemyType.Raider,
    2,
    2,
    400,
    500,
    1,
    2,
    ScriptWrap(LazySkeletonInit)
  );

  #endregion

  #region Spawns

  // Now that we've created our new enemy, let's register it into the spawn database!
  // This will allow the game to automatically spawn this enemy (or any object!) into
  // the world when resources spawn.
  globalvar SpawnLazySkeleton;
  SpawnLazySkeleton = SpawnCreate(undefined, SpawnType.Enemy, EnemyLazySkeleton, ScriptWrap(LazySkeletonEligible));

  #endregion

  #region Zones

  // This lets you create a new zone (like the void)
  globalvar ZoneEmpty;
  ZoneEmpty = ZoneCreate(
    undefined,
    "empty",
    ZoneType.Single,
    1344,
    1344,
    tilesGrasslands,
    tilesGrasslands2,
    tilesDetailsGrassland,
    [Spawn.Rock, Spawn.Tree, SpawnLazySkeleton]
  );

  #endregion

  #region NPCs

  // Creating NPC's with basic dialogue / quests is easy! Let's make a basic
  // fetch quest. Remember that all displayed text in Forager must be lowercase!
  globalvar NPCForager
  NPCForager = NPCCreate(undefined, "Forager");
  NPCQuestCreate(NPCForager, "CoalQuest");
  NPCQuestAddState(
    NPCForager,
    "CoalQuest",
    "Give",
    ["hello there!", "you look just like me!", "can you help me get some coal?"],
    "GetCoal",
    undefined,
    undefined
  );
  NPCQuestAddState(
    NPCForager,
    "CoalQuest",
    "GetCoal",
    ["wow, thanks so much!", "i'll give you my most prized item as a thank you."],
    undefined,
    [Item.Coal, 5],
    ScriptWrap(CompleteCoalQuest)
  );

  #endregion

#define PrintGreeting
  
  // For our command /greeting
  Trace("Hello world from PrintGreeting!");

#define UseMagicWand 

  // Now we're going to create a projectile. Since we can't create actual GameMaker
  // objects with mods, we'll instead base it off of an empty instance...
  var _orb = ModObjectSpawn(objPlayer.x, objPlayer.y, 0);
  
  // Now I'll set some stuff for this instance to make it into what we need!
  with (_orb) {
    image_blend = c_blue;
    sprite_index = sprCircle;
    direction = point_direction(objPlayer.x, objPlayer.y, mouse_x, mouse_y);
    InstanceAssignMethod(id, "step", ScriptWrap(MagicWandUpdate));
  }

#define MagicWandUpdate

  // Zap
  if (irandom_range(0,100) <= 50) {
    ZapSpawn();
  }

  // Move and collide
  var _xDelta = lengthdir_x(6, direction);
  var _yDelta = lengthdir_y(6, direction);
  var _inst = instance_place(x + _xDelta, y, parEnemy);
  if (_inst == noone) _inst = instance_place(x, y + _yDelta, parEnemy);
  if (_inst == noone) _inst = instance_place(x + _xDelta, y, parNatural);
  if (_inst == noone) _inst = instance_place(x, y + _yDelta, parNatural);
  if (_inst == noone) _inst = instance_place(x + _xDelta, y, parHerb);
  if (_inst == noone) _inst = instance_place(x, y + _yDelta, parHerb);
  if (_inst && !(object_is_ancestor(_inst.object_index, parSpecialObject))) {
    instance_destroy(id);
    ExplosionCreate(x, y, 64, false);
  }
  x += _xDelta;
  y += _yDelta;

#define LazySkeletonEligible

  // This script returns how many lazy skeletons should be added to the resource pool
  // that selects what resources to spawn at random
  if (irandom(100) < 5) return 1;

#define LazySkeletonInit
  sprIdle = sprSkeletonIdle;
  sprWalk = sprSkeletonWalk;
  sprWeapon = sprBone;

#define CompleteCoalQuest

  // This will fire when our NPC's quest calls its callback in the "GetCoal" state
  DropItem(x, y, Item.Poop, 1);

#define OnZoneGenerate

  // This script will fire when a zone is newly generated. Let's use it to spawn
  // some resoures 
  if (ZoneCurrent() == ZoneEmpty) {
    var _snapshotGrid = ResourceSnapshot();
    repeat (100) {
        ResourceSpawn(_snapshotGrid);
    }
    ds_grid_destroy(_snapshotGrid);
  }

  // Let's also spawn our NPC!
  var _npc = ModNPCSpawn(objPlayer.x, objPlayer.y, 0, NPCForager);
  with (_npc) {
    Trace(id);
    sprite_index = sprPlayerIdle;
    NPCSetQuest("CoalQuest");
    NPCSetState("Give");
  }


#define OnNewGame

  // At the start of a new save file, we'll save some test data...
  var _map = ModSaveDataFetch();
  _map[? "test"] = "hello world!";
  ModSaveDataSubmit(_map);

  // ...and we'll also send the player to our new zone!
  ZoneGoTo(ZoneEmpty);

#define OnLoad

  // Load that data we saved, and print it!
  var _map = ModSaveDataFetch();
  Trace(_map[? "test"]);